source ~/.config/nvim/plugins.vim
source ~/.config/nvim/settings.vim
source ~/.config/nvim/theme.vim

" Plugins settings
source ~/.config/nvim/plugins/lualine.vim
