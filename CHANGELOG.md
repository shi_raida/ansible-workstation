# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.1](https://gitlab.com/shi_raida/ansible-workstation/compare/v1.3.0...v1.3.1) (2025-01-03)


### Bug Fixes

* update packages name ([623b17a](https://gitlab.com/shi_raida/ansible-workstation/commit/623b17ad258e7ae4626c593ddc10b003ad341064))
* update thunderbird location ([3b8f0da](https://gitlab.com/shi_raida/ansible-workstation/commit/3b8f0daf27e3663f73cdaec4b34fffb4fbde6a97))


### Reverts

* disable syncthing ([2090e43](https://gitlab.com/shi_raida/ansible-workstation/commit/2090e43a4cd9759f1b28332c25115a6cf36360a5))

# [1.3.0](https://gitlab.com/shi_raida/ansible-workstation/compare/v1.2.1...v1.3.0) (2024-1-18)


### Features

* **apt:** add docker and podman ([13b0f12](https://gitlab.com/shi_raida/ansible-workstation/commit/13b0f12f626a20396c53679c9b874117a8a2d0e7))
* **snap:** add node ([5a99cb9](https://gitlab.com/shi_raida/ansible-workstation/commit/5a99cb92e084cf79aaa2ac8681e38817471965dc))

## [1.2.1](https://gitlab.com/shi_raida/ansible-workstation/compare/v1.2.0...v1.2.1) (2024-1-18)


### Bug Fixes

* **rust:** rustup not found ([aa571ce](https://gitlab.com/shi_raida/ansible-workstation/commit/aa571ceb0721e46fb153de3b8f5644942f09b5df))

# [1.2.0](https://gitlab.com/shi_raida/ansible-workstation/compare/v1.1.0...v1.2.0) (2024-1-9)


### Features

* **snap:** add just ([c082c83](https://gitlab.com/shi_raida/ansible-workstation/commit/c082c83700e29af10884be521cad9b2d9c2de416))
* **zsh:** add just completions ([f3298f3](https://gitlab.com/shi_raida/ansible-workstation/commit/f3298f3054f5901ee8d8d625a3eace3c865f80b9))

# [1.1.0](https://gitlab.com/shi_raida/ansible-workstation/compare/v1.0.1...v1.1.0) (2024-1-7)


### Features

* **i3:** add shortcut to change keyboard layout ([6a92bad](https://gitlab.com/shi_raida/ansible-workstation/commit/6a92bade03d325d592c3382040a5d2803773f15a))

## [1.0.1](https://gitlab.com/shi_raida/ansible-workstation/compare/v1.0.0...v1.0.1) (2024-1-5)

# 1.0.0 (2024-1-4)


### Bug Fixes

* **ci:** default branch name ([74e4c36](https://gitlab.com/shi_raida/ansible-workstation/commit/74e4c3621e2cfbfbfe5eedaf639fb9de6c5d8da6))
* **firefox:** define vars online when screen is present ([b3302ce](https://gitlab.com/shi_raida/ansible-workstation/commit/b3302ce8078a0f0501250a6d19daa3e255c7997a))
* **power-menu:** icons ([cd946b7](https://gitlab.com/shi_raida/ansible-workstation/commit/cd946b78d8fadc209f65871d258a9cc119778dcc))
* **run:** add ansible bin PATH ([a3c4e37](https://gitlab.com/shi_raida/ansible-workstation/commit/a3c4e372fea46a018c38d816024995d20833f27a))
* **run:** handle empty passwords ([c247649](https://gitlab.com/shi_raida/ansible-workstation/commit/c247649e8e2e0cacfbd2c8500b44fb8151361252))
* **scripts:** ansible installation directories ([a84ef19](https://gitlab.com/shi_raida/ansible-workstation/commit/a84ef19104bd7f329a2b4d841212d396ea35f332))
* **snap:** use classic channel ([b86bdcb](https://gitlab.com/shi_raida/ansible-workstation/commit/b86bdcbf63d7c0e81f766853a25ecd3bb612d045))
* **tasks:** check X.org ([998b94b](https://gitlab.com/shi_raida/ansible-workstation/commit/998b94b2b25fa6718a97902bf00c928cc66465d5))


### Features

* **apt:** add npm ([af8bd85](https://gitlab.com/shi_raida/ansible-workstation/commit/af8bd857b348a5c2ff875e9bf8e17b613e587cc2))
* **apt:** add virtualbox and vagrant ([86d1866](https://gitlab.com/shi_raida/ansible-workstation/commit/86d1866f50831bfdc5604ea131aa652150052a0a))
* **bluetooth:** restart service on changes ([df32bae](https://gitlab.com/shi_raida/ansible-workstation/commit/df32baef6a208393e2a83fe4f55048584956c51e))
* **ci:** init ([30bb24c](https://gitlab.com/shi_raida/ansible-workstation/commit/30bb24c5ee1f7fd24c25fa14f10a19a794c1b13f))
* **config:** task to copy basic config files ([3ff5e32](https://gitlab.com/shi_raida/ansible-workstation/commit/3ff5e32ab6c0ae82c854eda9baf75d1f97b0cc8b))
* **firefox:** copy prefs.js ([f5e6f2b](https://gitlab.com/shi_raida/ansible-workstation/commit/f5e6f2bf068e5a87c22e9a0541fe86d50dc54173))
* **git:** define default pull strategy ([af936d5](https://gitlab.com/shi_raida/ansible-workstation/commit/af936d5b86b84a4a16373c33011164fe5afce13f))
* **scripts:** ansible installation ([5c79a23](https://gitlab.com/shi_raida/ansible-workstation/commit/5c79a235678457544a4a22e1b7be5a3cc4a5bab6))
* **snap:** task to install snap packages ([04e3aa6](https://gitlab.com/shi_raida/ansible-workstation/commit/04e3aa66a99d05b001a92383f52c426fd49bf380))
* **tasks:** add alacritty and zsh ([e24456e](https://gitlab.com/shi_raida/ansible-workstation/commit/e24456eeac99354183987e3e457627b2b6c9b63d))
* **tasks:** add ansible ([5e8e703](https://gitlab.com/shi_raida/ansible-workstation/commit/5e8e703f28b90ec42d0483d7a221a137213ed0ee))
* **tasks:** add apt ([edb5d0a](https://gitlab.com/shi_raida/ansible-workstation/commit/edb5d0a0c702e925ab6296e5f864a17e2ad26c0e))
* **tasks:** add base programs ([ddc704f](https://gitlab.com/shi_raida/ansible-workstation/commit/ddc704ff56fa9747929a0cbfb7c320939470d920))
* **tasks:** add firefox ([ef4b6e8](https://gitlab.com/shi_raida/ansible-workstation/commit/ef4b6e823c5cbca1b352cee94b4590a26b2d4497))
* **tasks:** add fonts ([e13e7e0](https://gitlab.com/shi_raida/ansible-workstation/commit/e13e7e0fd6eeb4e0b0ad9f03b1be4b9edad2eade))
* **tasks:** add git ([8419cd5](https://gitlab.com/shi_raida/ansible-workstation/commit/8419cd5d755100c89bfc1f4eeaccfa93a3eb27ff))
* **tasks:** add i3 ([8e5dc19](https://gitlab.com/shi_raida/ansible-workstation/commit/8e5dc190157f230dea082e904b20b783d7075572))
* **tasks:** add neovim ([f05b9f5](https://gitlab.com/shi_raida/ansible-workstation/commit/f05b9f55091222b458b2b2039bc312b80e3c36b0))
* **tasks:** add obsidian ([ddfd6ee](https://gitlab.com/shi_raida/ansible-workstation/commit/ddfd6eef7134f3c12f8eb5ef080a285683713d5d))
* **tasks:** add rust ([039bf22](https://gitlab.com/shi_raida/ansible-workstation/commit/039bf22a82cc28e833995a7a757dd20724fc1e1b))
* **tasks:** add syncthing ([19f3ce7](https://gitlab.com/shi_raida/ansible-workstation/commit/19f3ce793741161c63de67a8fd20dd9f7037baf8))
* **tasks:** add thunderbird ([4062822](https://gitlab.com/shi_raida/ansible-workstation/commit/4062822c26d07101a899d80b3f9c3da8e0b2c42a))
* **tasks:** add user ([f9d023b](https://gitlab.com/shi_raida/ansible-workstation/commit/f9d023b5bed07774e0971a80d77c6c6e4a3a9124))
* **tasks:** add vscode ([87d41d7](https://gitlab.com/shi_raida/ansible-workstation/commit/87d41d7daed58685b261ca23d2422fbc0a84fdc4))
* **tasks:** check linux OS and not root user ([e320315](https://gitlab.com/shi_raida/ansible-workstation/commit/e320315f9a665b600a2b78bb134f0ef7443c0cac))

# 1.0.0-dev.1 (2024-1-4)


### Bug Fixes

* **ci:** default branch name ([74e4c36](https://gitlab.com/shi_raida/ansible-workstation/commit/74e4c3621e2cfbfbfe5eedaf639fb9de6c5d8da6))
* **firefox:** define vars online when screen is present ([b3302ce](https://gitlab.com/shi_raida/ansible-workstation/commit/b3302ce8078a0f0501250a6d19daa3e255c7997a))
* **power-menu:** icons ([cd946b7](https://gitlab.com/shi_raida/ansible-workstation/commit/cd946b78d8fadc209f65871d258a9cc119778dcc))
* **run:** add ansible bin PATH ([a3c4e37](https://gitlab.com/shi_raida/ansible-workstation/commit/a3c4e372fea46a018c38d816024995d20833f27a))
* **run:** handle empty passwords ([c247649](https://gitlab.com/shi_raida/ansible-workstation/commit/c247649e8e2e0cacfbd2c8500b44fb8151361252))
* **scripts:** ansible installation directories ([a84ef19](https://gitlab.com/shi_raida/ansible-workstation/commit/a84ef19104bd7f329a2b4d841212d396ea35f332))
* **snap:** use classic channel ([b86bdcb](https://gitlab.com/shi_raida/ansible-workstation/commit/b86bdcbf63d7c0e81f766853a25ecd3bb612d045))
* **tasks:** check X.org ([998b94b](https://gitlab.com/shi_raida/ansible-workstation/commit/998b94b2b25fa6718a97902bf00c928cc66465d5))


### Features

* **apt:** add npm ([af8bd85](https://gitlab.com/shi_raida/ansible-workstation/commit/af8bd857b348a5c2ff875e9bf8e17b613e587cc2))
* **apt:** add virtualbox and vagrant ([86d1866](https://gitlab.com/shi_raida/ansible-workstation/commit/86d1866f50831bfdc5604ea131aa652150052a0a))
* **bluetooth:** restart service on changes ([df32bae](https://gitlab.com/shi_raida/ansible-workstation/commit/df32baef6a208393e2a83fe4f55048584956c51e))
* **ci:** init ([30bb24c](https://gitlab.com/shi_raida/ansible-workstation/commit/30bb24c5ee1f7fd24c25fa14f10a19a794c1b13f))
* **config:** task to copy basic config files ([3ff5e32](https://gitlab.com/shi_raida/ansible-workstation/commit/3ff5e32ab6c0ae82c854eda9baf75d1f97b0cc8b))
* **firefox:** copy prefs.js ([f5e6f2b](https://gitlab.com/shi_raida/ansible-workstation/commit/f5e6f2bf068e5a87c22e9a0541fe86d50dc54173))
* **git:** define default pull strategy ([af936d5](https://gitlab.com/shi_raida/ansible-workstation/commit/af936d5b86b84a4a16373c33011164fe5afce13f))
* **scripts:** ansible installation ([5c79a23](https://gitlab.com/shi_raida/ansible-workstation/commit/5c79a235678457544a4a22e1b7be5a3cc4a5bab6))
* **snap:** task to install snap packages ([04e3aa6](https://gitlab.com/shi_raida/ansible-workstation/commit/04e3aa66a99d05b001a92383f52c426fd49bf380))
* **tasks:** add alacritty and zsh ([e24456e](https://gitlab.com/shi_raida/ansible-workstation/commit/e24456eeac99354183987e3e457627b2b6c9b63d))
* **tasks:** add ansible ([5e8e703](https://gitlab.com/shi_raida/ansible-workstation/commit/5e8e703f28b90ec42d0483d7a221a137213ed0ee))
* **tasks:** add apt ([edb5d0a](https://gitlab.com/shi_raida/ansible-workstation/commit/edb5d0a0c702e925ab6296e5f864a17e2ad26c0e))
* **tasks:** add base programs ([ddc704f](https://gitlab.com/shi_raida/ansible-workstation/commit/ddc704ff56fa9747929a0cbfb7c320939470d920))
* **tasks:** add firefox ([ef4b6e8](https://gitlab.com/shi_raida/ansible-workstation/commit/ef4b6e823c5cbca1b352cee94b4590a26b2d4497))
* **tasks:** add fonts ([e13e7e0](https://gitlab.com/shi_raida/ansible-workstation/commit/e13e7e0fd6eeb4e0b0ad9f03b1be4b9edad2eade))
* **tasks:** add git ([8419cd5](https://gitlab.com/shi_raida/ansible-workstation/commit/8419cd5d755100c89bfc1f4eeaccfa93a3eb27ff))
* **tasks:** add i3 ([8e5dc19](https://gitlab.com/shi_raida/ansible-workstation/commit/8e5dc190157f230dea082e904b20b783d7075572))
* **tasks:** add neovim ([f05b9f5](https://gitlab.com/shi_raida/ansible-workstation/commit/f05b9f55091222b458b2b2039bc312b80e3c36b0))
* **tasks:** add obsidian ([ddfd6ee](https://gitlab.com/shi_raida/ansible-workstation/commit/ddfd6eef7134f3c12f8eb5ef080a285683713d5d))
* **tasks:** add rust ([039bf22](https://gitlab.com/shi_raida/ansible-workstation/commit/039bf22a82cc28e833995a7a757dd20724fc1e1b))
* **tasks:** add syncthing ([19f3ce7](https://gitlab.com/shi_raida/ansible-workstation/commit/19f3ce793741161c63de67a8fd20dd9f7037baf8))
* **tasks:** add thunderbird ([4062822](https://gitlab.com/shi_raida/ansible-workstation/commit/4062822c26d07101a899d80b3f9c3da8e0b2c42a))
* **tasks:** add user ([f9d023b](https://gitlab.com/shi_raida/ansible-workstation/commit/f9d023b5bed07774e0971a80d77c6c6e4a3a9124))
* **tasks:** add vscode ([87d41d7](https://gitlab.com/shi_raida/ansible-workstation/commit/87d41d7daed58685b261ca23d2422fbc0a84fdc4))
* **tasks:** check linux OS and not root user ([e320315](https://gitlab.com/shi_raida/ansible-workstation/commit/e320315f9a665b600a2b78bb134f0ef7443c0cac))
