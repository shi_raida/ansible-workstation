# Ansible Workstation

An [Ansible] playbook to setup my [Ubuntu] workstation.


<!-- ================================ Links ================================ -->

[Ansible]: https://www.ansible.com/
[Ubuntu]: https://ubuntu.com/